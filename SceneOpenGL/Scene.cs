﻿using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace SceneOpenGL
{
    internal static class Scene
    {
        public static void DrawScene(double turretRotationAngle)
        {
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();

            GL.Rotate(0.0, Vector3d.UnitY);  // вид спереди
            GL.Rotate(0.0, Vector3d.UnitY); // вид сзади
            GL.Rotate(-10, Vector3d.UnitX);   // вид сверху

            GL.Rotate(turretRotationAngle, Vector3d.UnitY);

            DrawTable();
            DrawStools();
            DrawPyramidBase();
            DrawAnvil();

            GL.PopMatrix();
        }

        private static void DrawTable()
        {
            GL.BindTexture(TextureTarget.Texture2D, Render.TextureId[0]);

            GL.PushMatrix();
            ShapeDrawer.DrawParallelepiped(0.0, 0.0, 0.0, 0.35, 0.01, 0.19, true);

            ShapeDrawer.DrawParallelepiped(0.01, -0.2, 0.01, 0.01, 0.2, 0.009, true);

            ShapeDrawer.DrawParallelepiped(0.01, -0.2, 0.17, 0.01, 0.2, 0.009, true);

            ShapeDrawer.DrawParallelepiped(0.33, -0.2, 0.17, 0.01, 0.2, 0.009, true);

            ShapeDrawer.DrawParallelepiped(0.33, -0.2, 0.01, 0.01, 0.2, 0.009, true);

            GL.PopMatrix();
        }


        private static void DrawStools()
        {
            GL.BindTexture(TextureTarget.Texture2D, Render.TextureId[1]);
            GL.MatrixMode(MatrixMode.Modelview);
            GL.PushMatrix();
            GL.Translate(-0.1, -0.05, 0.1);
            DrawStoolBase();
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Translate(0.42, -0.05, 0.1);
            DrawStoolBase();
            GL.PopMatrix();
        }

        private static void DrawStoolBase()
        {

            GL.PushMatrix();
            GL.Rotate(90.0, Vector3d.UnitX);
            ShapeDrawer.DrawCylinder(0.058, 0.005, 0.0, 0.15, 0.1, true);
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Rotate(90.0, Vector3d.UnitX);
            GL.Translate(0.028, 0.028, 0);
            ShapeDrawer.DrawCylinder(0.0045, 0.15, 0.0, 0.0, 0.1, true);
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Rotate(90.0, Vector3d.UnitX);
            GL.Translate(-0.028, 0.028, 0);
            ShapeDrawer.DrawCylinder(0.0045, 0.15, 0.0, 0.0, 0.1, true);
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Rotate(90.0, Vector3d.UnitX);
            GL.Translate(-0.028, -0.028, 0);
            ShapeDrawer.DrawCylinder(0.0045, 0.15, 0.0, 0.0, 0.1, true);
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Rotate(90.0, Vector3d.UnitX);
            GL.Translate(0.028, -0.028, 0);
            ShapeDrawer.DrawCylinder(0.0045, 0.15, 0.0, 0.0, 0.1, true);
            GL.PopMatrix();

        }

        private static void DrawPyramidBase()
        {
            GL.BindTexture(TextureTarget.Texture2D, Render.TextureId[2]);
            // Египетская пирамида
            GL.PushMatrix();
            GL.Translate(0.3570, -0.035, 0.4);
            GL.Rotate(-25, Vector3d.UnitY);
            GL.Translate(-0.7, 0.02, -0.7);
            ShapeDrawer.DrawClippedPyramid(0.0, 0.2, 0.2);
            GL.PopMatrix();
        }

        private static void DrawAnvil()
        {
            GL.PushMatrix();
            GL.Translate(-0.6, -0.274, 0);
            Anvil();
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Translate(-0.58, -0.42, 0);
            DrawHammer();
            GL.PopMatrix();
        }


        private static void Anvil()
        {
            GL.BindTexture(TextureTarget.Texture2D, Render.TextureId[3]);
            GL.PushMatrix();

            ShapeDrawer.DrawParallelepiped(0.2, 0.2, 0, 0.2, 0.03, 0.13, true);
            ShapeDrawer.DrawParallelepiped(0.2, 0.105, 0.014, 0.15, 0.1, 0.1, true);
            ShapeDrawer.DrawParallelepiped(0.19, 0.107, 0.007, 0.17, 0.02, 0.115, true);
            ShapeDrawer.DrawParallelepiped(0.18, 0.07, 0, 0.19, 0.04, 0.13, true);
            GL.PopMatrix();

            GL.PushMatrix();
            GL.Translate(0.15, 0, 0.2);
            GL.Rotate(76, Vector3d.UnitZ);
            GL.Translate(0.195, 0.06, -0.137);
            ShapeDrawer.DrawClippedPyramid(0.0, 0.075, 0.04);
            GL.PopMatrix();
        }

        private static void DrawHammer()
        {
            GL.PushMatrix();
            GL.Rotate(10.1, Vector3d.UnitZ);
            Hammer();
            GL.PopMatrix();
        }

        private static void Hammer()
        {
            GL.BindTexture(TextureTarget.Texture2D, Render.TextureId[4]);
            // Кувалда
            GL.PushMatrix();
            GL.Translate(0.26, 0.35, 0.05);
            ShapeDrawer.DrawCylinder(0.019, 0.1326 / 2, 0.0, 1.0, 0.1, true);
            GL.PopMatrix();

            GL.BindTexture(TextureTarget.Texture2D, Render.TextureId[0]);
            // Рукоять
            GL.PushMatrix();
            GL.Rotate(-13.5, Vector3d.UnitZ);
            GL.Rotate(90.0, Vector3d.UnitY);
            GL.Translate(-0.085, 0.403, 0.18);
            ShapeDrawer.DrawCylinder(0.005, 0.205, 0.0, 0.0, 0.1, true);
            GL.PopMatrix();

            // Крепление кувалды
            GL.PushMatrix();
            GL.Rotate(-12.5, Vector3d.UnitZ);
            GL.Rotate(90.0, Vector3d.UnitY);
            GL.Translate(-0.085, 0.4, 0.18);
            ShapeDrawer.DrawCylinder(0.008, 0.035, 0.0, 0.0, 0.1, true);
            GL.PopMatrix();
        }
    }
}
